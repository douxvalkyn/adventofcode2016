package outils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import day01.Day01;
import day02.Day02;
import day03.Day03;
import day04.Day04;
import day05.Day05;
import day06.Day06;
import day07.Day07;
import day08.Day08;
import day09.Day09;
import day10.Day10;
import day11.Day11;
import day12.Day12;
import day13.Day13;
import day14.Day14;
import day15.Day15;
import day16.Day16;
import day17.Day17;
import day18.Day18;
import day19.Day19;
import day20.Day20;
import day21.Day21;
import day22.Day22;
import day23.Day23;
import day24.Day24;
import day25.Day25;

public class Timings {
	
	private static final String ANSI_RESET="\u001B[0m";
	private static final String ANSI_RED="\u001B[31m";
	private static final String ANSI_GREEN="\u001B[32m";
	private static final String ANSI_BRIGHT_GREEN="\u001B[92m";
	private static final String ANSI_YELLOW="\u001B[33m";
	private static final String ANSI_PURPLE="\u001B[35m";
	private static final String ANSI_BLUE="\u001B[34m";
	private static final String ANSI_BRIGHT_MAGENTA="\u001B[95m";
	private static final String  YELLOW_BRIGHT = "\033[0;93m";
	


	public static void main(String[] args) throws IOException {
		List<Long> dureesRun1 = new ArrayList<Long>();
		List<Long> dureesRun2 = new ArrayList<Long>();

		//Day01
		System.out.println("----- DAY 01 -----");
		Day01 day01 = new Day01();
		long start = System.currentTimeMillis();
		day01.run1();
		long duree = System.currentTimeMillis() - start;		
		dureesRun1.add(duree);
		start = System.currentTimeMillis();
		day01.run2();
		long duree2 = System.currentTimeMillis() - start;
		dureesRun2.add(duree2);
		
		//Day02
		System.out.println("----- DAY 02 -----");
		Day02 day02 = new Day02();
		start = System.currentTimeMillis();
		day02.run1();
		duree = System.currentTimeMillis() - start;		
		dureesRun1.add(duree);
		start = System.currentTimeMillis();
		day02.run2();
		duree2 = System.currentTimeMillis() - start;
		dureesRun2.add(duree2);
		
		//Day03
		System.out.println("----- DAY 03 -----");
		Day03 day03 = new Day03();
		start = System.currentTimeMillis();
		day03.run1();
		duree = System.currentTimeMillis() - start;		
		dureesRun1.add(duree);
		start = System.currentTimeMillis();
		day03.run2();
		duree2 = System.currentTimeMillis() - start;
		dureesRun2.add(duree2);
		
		//Day04
		System.out.println("----- DAY 04 -----");
		Day04 day04 = new Day04();
		start = System.currentTimeMillis();
		day04.run1();
		duree = System.currentTimeMillis() - start;		
		dureesRun1.add(duree);
		start = System.currentTimeMillis();
		day04.run2();
		duree2 = System.currentTimeMillis() - start;
		dureesRun2.add(duree2);
		
		//Day05
		System.out.println("----- DAY 05 -----");
		Day05 day05 = new Day05();
		start = System.currentTimeMillis();
		day05.run1();
		duree = System.currentTimeMillis() - start;		
		dureesRun1.add(duree);
		start = System.currentTimeMillis();
		day05.run2();
		duree2 = System.currentTimeMillis() - start;
		dureesRun2.add(duree2);
		
		//Day06
		System.out.println("----- DAY 06 -----");
		Day06 day06 = new Day06();
		start = System.currentTimeMillis();
		day06.run1();
		duree = System.currentTimeMillis() - start;		
		dureesRun1.add(duree);
		start = System.currentTimeMillis();
		day06.run2();
		duree2 = System.currentTimeMillis() - start;
		dureesRun2.add(duree2);
		
		//Day07
		System.out.println("----- DAY 07 -----");
		Day07 day07 = new Day07();
		start = System.currentTimeMillis();
		day07.run1();
		duree = System.currentTimeMillis() - start;		
		dureesRun1.add(duree);
		start = System.currentTimeMillis();
		day07.run2();
		duree2 = System.currentTimeMillis() - start;
		dureesRun2.add(duree2);
		
		//Day08
		System.out.println("----- DAY 08 -----");
		Day08 day08 = new Day08();
		start = System.currentTimeMillis();
		day08.run1();
		duree = System.currentTimeMillis() - start;		
		dureesRun1.add(duree);
		start = System.currentTimeMillis();
		day08.run2();
		duree2 = System.currentTimeMillis() - start;
		dureesRun2.add(duree2);
		
		//Day09
		System.out.println("----- DAY 09 -----");
		Day09 day09 = new Day09();
		start = System.currentTimeMillis();
		day09.run1();
		duree = System.currentTimeMillis() - start;		
		dureesRun1.add(duree);
		start = System.currentTimeMillis();
		day09.run2();
		duree2 = System.currentTimeMillis() - start;
		dureesRun2.add(duree2);
	
		//Day10
		System.out.println("----- DAY 10 -----");
		Day10 day10 = new Day10();
		start = System.currentTimeMillis();
		day10.run1();
		duree = System.currentTimeMillis() - start;		
		dureesRun1.add(duree);
		start = System.currentTimeMillis();
		day10.run2();
		duree2 = System.currentTimeMillis() - start;
		dureesRun2.add(duree2);
		
		//Day11
		System.out.println("----- DAY 11 -----");
		Day11 day11 = new Day11();
		start = System.currentTimeMillis();
//		day11.run1();
//		duree = System.currentTimeMillis() - start;		
		duree=7594000;
		dureesRun1.add(duree);
		start = System.currentTimeMillis();
//		day11.run2();
//		duree2 = System.currentTimeMillis() - start;
		duree2=7594000;
		dureesRun2.add(duree2);
		
		//Day12
		System.out.println("----- DAY 12 -----");
		Day12 day12 = new Day12();
		start = System.currentTimeMillis();
		day12.run1();
		duree = System.currentTimeMillis() - start;		
		dureesRun1.add(duree);
		start = System.currentTimeMillis();
		day12.run2();
		duree2 = System.currentTimeMillis() - start;
		dureesRun2.add(duree2);
		
		//Day13
		System.out.println("----- DAY 13 -----");
		Day13 day13 = new Day13();
		start = System.currentTimeMillis();
		day13.run1();
		duree = System.currentTimeMillis() - start;		
		dureesRun1.add(duree);
		start = System.currentTimeMillis();
		day13.run2();
		duree2 = System.currentTimeMillis() - start;
		dureesRun2.add(duree2);
		
		//Day14
		System.out.println("----- DAY 14 -----");
		Day14 day14 = new Day14();
		start = System.currentTimeMillis();
		day14.run1();
		duree = System.currentTimeMillis() - start;		
		dureesRun1.add(duree);
		start = System.currentTimeMillis();
		//day14.run2();
		duree2 = System.currentTimeMillis() - start;
		duree2=1008714;
		dureesRun2.add(duree2);
		
		//Day15
		System.out.println("----- DAY 15 -----");
		Day15 day15 = new Day15();
		start = System.currentTimeMillis();
		day15.run1();
		duree = System.currentTimeMillis() - start;		
		dureesRun1.add(duree);
		start = System.currentTimeMillis();
		//day15.run2();
		duree2 = System.currentTimeMillis() - start;
		duree2 = 1005387;
		dureesRun2.add(duree2);
		
		//Day16
		System.out.println("----- DAY 16 -----");
		Day16 day16 = new Day16();
		start = System.currentTimeMillis();
		day16.run1();
		duree = System.currentTimeMillis() - start;		
		dureesRun1.add(duree);
		start = System.currentTimeMillis();
		day16.run2();
		duree2 = System.currentTimeMillis() - start;
		dureesRun2.add(duree2);
		
		//Day17
		System.out.println("----- DAY 17 -----");
		Day17 day17 = new Day17();
		start = System.currentTimeMillis();
		day17.run1();
		duree = System.currentTimeMillis() - start;		
		dureesRun1.add(duree);
		start = System.currentTimeMillis();
		day17.run2();
		duree2 = System.currentTimeMillis() - start;
		dureesRun2.add(duree2);
		
		//Day18
		System.out.println("----- DAY 18 -----");
		Day18 day18 = new Day18();
		start = System.currentTimeMillis();
		day18.run1();
		duree = System.currentTimeMillis() - start;		
		dureesRun1.add(duree);
		start = System.currentTimeMillis();
		day18.run2();
		duree2 = System.currentTimeMillis() - start;
		dureesRun2.add(duree2);
		
		//Day19
		System.out.println("----- DAY 19 -----");
		Day19 day19 = new Day19();
		start = System.currentTimeMillis();
		//day19.run1();
		//duree = System.currentTimeMillis() - start;	
		duree=32526000;
		dureesRun1.add(duree);
		start = System.currentTimeMillis();
		//day19.run2();
		duree2=141096700;
		//duree2 = System.currentTimeMillis() - start;
		dureesRun2.add(duree2);
		
		//Day20
		System.out.println("----- DAY 20 -----");
		Day20 day20 = new Day20();
		start = System.currentTimeMillis();
		day20.run1();
		duree = System.currentTimeMillis() - start;		
		dureesRun1.add(duree);
		start = System.currentTimeMillis();
		day20.run2();
		duree2 = System.currentTimeMillis() - start;
		dureesRun2.add(duree2);
		
		//Day21
		System.out.println("----- DAY 21 -----");
		Day21 day21 = new Day21();
		start = System.currentTimeMillis();
		day21.run1();
		duree = System.currentTimeMillis() - start;		
		dureesRun1.add(duree);
		start = System.currentTimeMillis();
		day21.run2();
		duree2 = System.currentTimeMillis() - start;
		dureesRun2.add(duree2);
		
		//Day22
		System.out.println("----- DAY 22 -----");
		Day22 day22 = new Day22();
		start = System.currentTimeMillis();
		day22.run1();
		duree = System.currentTimeMillis() - start;		
		dureesRun1.add(duree);
		start = System.currentTimeMillis();
		day22.run2();
		duree2 = System.currentTimeMillis() - start;
		dureesRun2.add(duree2);
		
		//Day23
		System.out.println("----- DAY 23 -----");
		Day23 day23 = new Day23();
		start = System.currentTimeMillis();
		day23.run1();
		duree = System.currentTimeMillis() - start;		
		dureesRun1.add(duree);
		start = System.currentTimeMillis();
		//day23.run2();
		duree2 = System.currentTimeMillis() - start;
		//duree2=2232000;
		dureesRun2.add(duree2);
		
		//Day24
		System.out.println("----- DAY 24 -----");
		Day24 day24 = new Day24();
		start = System.currentTimeMillis();
		day24.run1();
		duree = System.currentTimeMillis() - start;		
		dureesRun1.add(duree);
		start = System.currentTimeMillis();
		day24.run2();
		duree2 = System.currentTimeMillis() - start;
		dureesRun2.add(duree2);
		
		//Day25
		System.out.println("----- DAY 25 -----");
		Day25 day25 = new Day25();
		start = System.currentTimeMillis();
		day25.run1();
		duree = System.currentTimeMillis() - start;		
		dureesRun1.add(duree);
		start = System.currentTimeMillis();
		day25.run2();
		duree2 = System.currentTimeMillis() - start;
		dureesRun2.add(duree2);
		
		
		
		List<String> daysAffichage = new ArrayList<String>();
		daysAffichage.add("1 ");		daysAffichage.add("2 ");		daysAffichage.add("3 ");		daysAffichage.add("4 ");		daysAffichage.add("5 ");
		daysAffichage.add("6 ");		daysAffichage.add("7 ");		daysAffichage.add("8 ");		daysAffichage.add("9 ");		daysAffichage.add("10 ");
		daysAffichage.add("11 ");		daysAffichage.add("12 ");		daysAffichage.add("13 ");		daysAffichage.add("14 ");		daysAffichage.add("15 ");
		daysAffichage.add("16 ");		daysAffichage.add("17 ");		daysAffichage.add("18 ");		daysAffichage.add("19 ");		daysAffichage.add("20 ");
		daysAffichage.add("21 ");		daysAffichage.add("22 ");		daysAffichage.add("23 ");		daysAffichage.add("24 ");		daysAffichage.add("25 ");

		System.out.println("-----------------------------------------------------------------");
		System.out.println("-----------------------"+ANSI_BLUE+"Advent of Code 2016"+ANSI_RESET+"-----------------------");
		System.out.println("-----------------------------"+ANSI_BLUE+"Timings"+ANSI_RESET+"-----------------------------");
		System.out.println("-----------------------------------------------------------------");
		affichageCouleurDays2(affichageCouleurDays1(dureesRun1, dureesRun2,0), daysAffichage);
		daysAffichage.forEach(System.out::print);
		System.out.println();
		System.out.println("-----------------------------------------------------------------");
		for (int i=0; i<dureesRun1.size();i++) {
			long calcul1 = dureesRun1.get(i);
			String calculString1 = ""+calcul1;
			long calcul2 = dureesRun2.get(i);
			String calculString2 = ""+calcul2;
			if (i<9) {
			System.out.println("[ Day"+(i+1)+" -  Run1: " + StringUtils.leftPad(colored(calculString1), 19, ".") + " ms - Run2: " + StringUtils.leftPad(colored(calculString2), 19, ".") + " ms]]");
			}
			if (i>=9) {
			System.out.println("[ Day"+(i+1)+" - Run1: " + StringUtils.leftPad(colored(calculString1), 19, ".") + " ms - Run2: " + StringUtils.leftPad(colored(calculString2), 19, ".") + " ms]]");
			}
		}
		long sum = 0;
		for (long dur : dureesRun1) {
			sum=sum+dur;
		}
		for (long dur : dureesRun2) {
			sum=sum+dur;
		}
		long sumSecondes=sum/1000;
		long minutes=sumSecondes/60;
		long heures=minutes/60;
		System.out.println("[ Total:      " + sum + " ms soit "+ sumSecondes +" s soit "+minutes + " min soit "+ heures+" h ]");
	}


	
	private static void affichageCouleurDays2(List<Integer> affichageCouleurDays, List<String> daysAffichage) {
		for (int i=0;i<daysAffichage.size();i++) {
			if (affichageCouleurDays.size()>i) {
			if (affichageCouleurDays.get(i)==1) {
				daysAffichage.set(i, YELLOW_BRIGHT+daysAffichage.get(i)+ANSI_RESET);
			}
			if (affichageCouleurDays.get(i)==2) {
				daysAffichage.set(i, ANSI_BRIGHT_GREEN+daysAffichage.get(i)+ANSI_RESET);
			}
			if (affichageCouleurDays.get(i)==0) {
				daysAffichage.set(i, ANSI_RED+daysAffichage.get(i)+ANSI_RESET);
			}
		}
		}
	}



	private static List<Integer> affichageCouleurDays1(List<Long> dureesRun1, List<Long> dureesRun2, int seuil) {
		List<Integer> daysWinOrLoose = new ArrayList<Integer>();
		for (int i=0; i<dureesRun1.size();i++) {
			long calcul1 = dureesRun1.get(i);
			long calcul2 = dureesRun2.get(i);
			
			if (calcul1 >= seuil && calcul2 < seuil) {
				daysWinOrLoose.add(1);
			}
			if (calcul1 < seuil && calcul2 >= seuil) {
				daysWinOrLoose.add(1);
			}
			if (calcul1 >= seuil && calcul2 >= seuil) {
				daysWinOrLoose.add(2);
			}
			if (calcul1 < seuil && calcul2 < seuil) {
				daysWinOrLoose.add(0);
			}
			
		}
		return daysWinOrLoose;
	}


	private static String colored(String calculString) {
	String res = "";
		if (Integer.parseInt(calculString)<1000) {
			res=ANSI_BRIGHT_GREEN+calculString+ANSI_RESET;
		}
		if (Integer.parseInt(calculString)>=1000 & Integer.parseInt(calculString)<10000) {
			res=ANSI_GREEN+calculString+ANSI_RESET;
		}
		if (Integer.parseInt(calculString)>=10000 & Integer.parseInt(calculString)<100000) {
			res=ANSI_YELLOW+calculString+ANSI_RESET;
		}
		if (Integer.parseInt(calculString)>=100000 & Integer.parseInt(calculString)<1000000) {
			res=ANSI_RED+calculString+ANSI_RESET;
		}
		if (Integer.parseInt(calculString)>=1000000 & Integer.parseInt(calculString)<10000000) {
			res=ANSI_PURPLE+calculString+ANSI_RESET;
		}
		if (Integer.parseInt(calculString)>1000000) {
			res=ANSI_BRIGHT_MAGENTA+calculString+ANSI_RESET;
		}
		return res;
	}

}
